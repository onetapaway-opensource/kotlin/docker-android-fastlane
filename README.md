# docker-android-fastlane

![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F51390426%2Frepository%2Ftags)

## Usage 
Place this at the top of your `.gitlab-ci` file
```
image: registry.gitlab.com/onetapaway-opensource/kotlin/docker-android-fastlane:34.1
```

## Contributing
Make any changes that you deem necessary and open an MR. Please explain why you made the changes you did in the MR. A Maintainer will review and merge it. 

## Maintainers
- Create a [new tag](https://gitlab.com/onetapaway-opensource/kotlin/docker-android-fastlane/-/tags/new) with an incremented version number
- Wait for the pipeline to succeed
- Update the `Usage` section in this README to match the new tag version

